##################################################
#                                                #
#  AEF FINAL EXAM R-SCRIPT                       #
#                                                #
#  EXAM ID 27 and 67                             #
#                                                #
#  The following script produces all output      #
#  used in the report. On our computers, the     #
#  entire script takes approx. 1 minute to run.  #
#                                                #
##################################################


# Set working directory to source file location
setwd(dirname(rstudioapi::getActiveDocumentContext()$path)) 

# Load packages
library("tidyverse")
library("lubridate")
library("ggplot2")
library("egg")
library("gridExtra")
library("rugarch")

#Load data
# NOTE MISSING OBSERVATIONS! E.G. NO OBSERVATION FOR 1998-07-01 14:22:00
spy_data <- read_rds("spy_data.rds")


### EXERCISE 1

# Create date-column from timestamp
spy_data$date <- date(spy_data$ts)

rv <- spy_data %>% group_by(date) %>% mutate(return=(log(price)-log(lag(price)))) %>% select(-price)
rv <- rv %>% group_by(date) %>% summarise(realized_variance = sum(return^2,na.rm=T), rv = sqrt(realized_variance)) 
#colnames(rv) <- c("date","realized_var")

# Calculate standard deviation for all (mimnute) observations for each date

# pull last price of the day for each date
close_price <- aggregate(. ~ date, spy_data[-1], last)

# calculate returns
returns <- close_price %>% 
  mutate(return = log(price)-log(lag(price))) %>%
  select(date,return)

# Plot and arrange time series (note that we multipl by 100 for return and rv)
price_plot <- ggplot(close_price, aes(x=date, y=price)) + geom_line() + xlab("") + ylab("Daily Closing Price") + theme_article()
volatility_plot <- ggplot(rv, aes(x=date, y=rv*100)) + geom_line() + xlab("") + ylab("Realized Volatility (Std. Dev.)") + theme_article()
returns_plot <- ggplot(returns, aes(x=date, y=return*100)) + geom_line() + xlab("") + ylab("Daily Log Return (~%)") + theme_article()
all_plots <- grid.arrange(price_plot, returns_plot, volatility_plot, ncol=3)
ggsave("all_plots.pdf",plot=all_plots,width = 27, height = 7, units= "cm", dpi = 300)

# Summary statistics
round(mean(returns$return,na.rm=T)*100,2)
round(sd(returns$return,na.rm=T)*100,2)
round(min(returns$return,na.rm=T)*100,2)
round(max(returns$return,na.rm=T)*100,2)

round(mean(rv$rv,na.rm=T)*100,2)
round(sd(rv$rv,na.rm=T)*100,2)
round(min(rv$rv,na.rm=T)*100,2)
round(max(rv$rv,na.rm=T)*100,2)

### EXERCISE 2

# Rolling window estimation
window_length <- 250
periods <- nrow(close_price) - window_length

cond_vars_rolling <- matrix(NA, nrow = periods, ncol = 1) %>% 
  `colnames<-`("cond_var")

for (i in 1:periods) {
  window <- returns[i : (i + window_length -1), ]
  cond_var <- window %>% na.omit() %>% pull(return) %>% var()
  cond_vars_rolling[i, ] <- cond_var
}

cond_vars <- cbind(as.tibble(close_price)[,1],c(rep(NA,window_length),cond_vars_rolling))


# GARCH
# ugarchspec specifies a standard garch(1,1) if no specification is given. That's what we want.
# The mean model is an ARMA(1,1), but since we only want cond. var., that does not matter.
garch_1_1 <- ugarchspec()
garch_fit = ugarchfit(garch_1_1, returns$return[-1], solver = 'hybrid')
cond_var_garch <- sigma(garch_fit)^2 # Squared to go from std. dev. to var. (Stefan forgets to do this in his exercise on Bookdown... :D)

# bind GARCH cond. var. to cond. vars. NA first because this is on returns, which do not have first value
cond_vars <- cbind(cond_vars,c(NA,cond_var_garch))
colnames(cond_vars) <- c("date","cond_var_rolling","cond_var_garch")
row.names(cond_vars) <- NULL

#Plot the rolling and GARCH cond. variances
cond_var_plot <- ggplot(cond_vars, aes(x = date)) + geom_line(aes(y = 100*sqrt(cond_var_garch)),size=0.2) + geom_line(aes(y = 100*sqrt(cond_var_rolling)),size=1) + 
  xlab("") + ylab("Cond. Volatility (Std. Dev.)") + theme_article()

ggsave("cond_var.pdf",plot=cond_var_plot,width = 18, height = 6, units= "cm", dpi = 300)

### EXERCISE 3
equity_premium_data <- read_rds("equity_premium_data.rds") #rp_div_lead is the equity premium one-month ahead

predictors <- equity_premium_data %>% 
  select(-date)

### 3.1 - NO CODE


### 3.2

# Rolling window

# Define length for estimation of parameters as well as length of out of sample
window_length <- 100
OOS_periods <- nrow(equity_premium_data) - window_length

# Matrix to collect equity premium
OOS_equity_premium <- matrix(NA, nrow = OOS_periods, ncol = 1) %>% 
  `colnames<-`("equity_premium")

OOS_equity_premium_manuel <- matrix(NA, nrow = OOS_periods, ncol = 1) %>% 
  `colnames<-`("equity_premium")

for(i in 1:OOS_periods){
  
  return_window <- predictors[i : (i + window_length -1), ] #Define the last 157 returns available up to date
  
  OLS <- lm(rp_div_lead ~ tms + dfy + bm + svar, data = return_window) # OLS regression based on last 157 returns
  
  # Equity premium prediction - with predict() and manually 
  prediction <- predict(OLS, predictors[i + window_length, ])
  
  # Check
  prediction_manuel <-  OLS$coefficients[1] + OLS$coefficients[2] * predictors[i + window_length, 2] + OLS$coefficients[3] * predictors[i + window_length, 3] + 
    OLS$coefficients[4] * predictors[i + window_length, 4] + OLS$coefficients[5] * predictors[i + window_length, 5]
  
  
  # Store predictions
  OOS_equity_premium[i, ] <- prediction
  OOS_equity_premium_manuel[i, ] <- as.numeric(prediction_manuel)
  
  
}
# We get same results with predict() and manually

# Gather our 200 predictions as well as the actual equity premiums
pred_actual_equity_premium <- cbind(equity_premium_data[101:357, 1], OOS_equity_premium) %>% 
  cbind(predictors[101:357 , 1])

# Change to numeric class
#pred_actual_equity_premium[, 2] <- as.numeric(pred_actual_equity_premium[, 2])

#Plot the predictions and actualy equity premiums
pred_actual_equity_premium_plot <- ggplot(pred_actual_equity_premium, aes(x = date)) + geom_line(aes(y = equity_premium), size = 1) + geom_line(aes(y = rp_div_lead),size=0.4) + 
  xlab("") + ylab("Equity Premium (%-pts.)") + theme_article()

ggsave("pred_actual_equity_premium_plot.pdf", plot = pred_actual_equity_premium_plot,width = 18, height = 6, units= "cm", dpi = 300)

### EXERCISE 4

# Fix parameters
gamma <- 4
r_f <- 0
mu <- returns$return %>% mean(na.rm=T)
mu <- 250*mu # Annualized compunded return, assuming 250 trading days per year

# Collect all volatility daily (conditional) measures and annulize
volatility_measures <- cbind(cond_vars,rv$realized_variance) %>% rename("realized_variance"="rv$realized_variance") %>%
  mutate_at(c("cond_var_garch","cond_var_rolling","realized_variance"), ~.*250) %>% na.omit()

# Compute alphas (alpha = 1/gamma * (mu-rf)/variance c.f. report)
all_alphas <- cbind(volatility_measures$date,(1/gamma)*(mu-r_f)/volatility_measures[2:4])
colnames(all_alphas) <- c("date","alphas_rolling","alphas_garch","alphas_rv")

# Reset index - OBS: both now start at date 1999-06-29
row.names(all_alphas) <- NULL
row.names(volatility_measures) <- NULL

alpha_rolling_plot <- ggplot(all_alphas) + geom_line(aes(x=date,y=alphas_rolling)) + xlab("") + ylab("Roll.") + theme_article()
alpha_garch_plot <- ggplot(all_alphas) + geom_line(aes(x=date,y=alphas_garch)) + xlab("") + ylab("Garch") + theme_article()
alpha_rv_plot <- ggplot(all_alphas) + geom_line(aes(x=date,y=alphas_rv)) + xlab("") + ylab("RV") + theme_article()

all_alphas_plot <- grid.arrange(alpha_rolling_plot, alpha_garch_plot, alpha_rv_plot, ncol=1)
ggsave("alphas_plot1.pdf",plot=all_alphas_plot,width = 18, height = 9, units= "cm", dpi = 300) 

# Filter to only get returns 250 periods after start (due to rolling window)
relevant_returns <- returns %>% filter(date>="1999-06-29") %>%  select(-date)

# element wise multiplication of alpha weights and actual returns
alpha_returns_rolling <- all_alphas$alphas_rolling * relevant_returns 
alpha_returns_garch <- all_alphas$alphas_garch * relevant_returns
alpha_returns_rv <- all_alphas$alphas_rv * relevant_returns

# calc. sharpe ratios and annualize (*sqrt(250))
sharpe_rolling <- (mean(alpha_returns_rolling$return)/sd(alpha_returns_rolling$return))*sqrt(250)
sharpe_garch <- (mean(alpha_returns_garch$return)/sd(alpha_returns_garch$return))*sqrt(250)
sharpe_rv <- (mean(alpha_returns_rv$return)/sd(alpha_returns_rv$return))*sqrt(250)

######### ESTIMATION WHEN MU CHANGES DYNAMICALLY EVERY MONTH

# Initiate tibble to hold mu's. For now just get dates and months for use immediately
predicted_mu <- returns %>% filter(date>="1999-06-29") %>% select(date)
predicted_mu$month <- floor_date(predicted_mu$date, "month")

# Lag monthly predicted returns by 1 to align predictions with the actual month they correspond to
pred_actual_equity_premium_leaded <- pred_actual_equity_premium %>% select(date,equity_premium)
pred_actual_equity_premium_leaded$equity_premium <- lag(pred_actual_equity_premium_leaded$equity_premium)

# Merge monthly (lagged) predicted returns onto all dates
predicted_mu <- left_join(predicted_mu,pred_actual_equity_premium_leaded,by=c("month"="date")) %>% select(date,equity_premium)

# Initialize matrx to collect alphas
all_alphas_dynamic <- matrix(NA, nrow = nrow(volatility_measures), ncol = 3) %>% 
  `colnames<-`(c("alpha_rolling","alpha_garch","alpha_rv"))

# Loop over each day and calculate alphas
for (i in 1:nrow(volatility_measures)){
  
  mu_dynamic <- predicted_mu[i,2]/100 # Divide by 100 because equity premium return given in percentage in exc. 3
  sigma_sq_rolling <- volatility_measures[i,2]
  sigma_sq_garch <- volatility_measures[i,3]
  sigma_sq_rv <- volatility_measures[i,4]
  
  alpha_rolling <- (1/gamma)*((mu_dynamic-r_f)/sigma_sq_rolling)
  alpha_garch <- (1/gamma)*((mu_dynamic-r_f)/sigma_sq_garch)
  alpha_rv <- (1/gamma)*((mu_dynamic-r_f)/sigma_sq_rv)
  
  #250 to start from top (because i starts at 251)
  all_alphas_dynamic[i, 1] <- alpha_rolling
  all_alphas_dynamic[i, 2] <- alpha_garch
  all_alphas_dynamic[i, 3] <- alpha_rv
}
all_alphas_dynamic <- cbind(as.tibble(volatility_measures)[1:nrow(volatility_measures),1],all_alphas_dynamic)

# Plot alphas
alpha_rolling_dynamic_plot <- ggplot(all_alphas_dynamic) + geom_line(aes(x=date,y=alpha_rolling)) + xlab("") + ylab("Roll.") + theme_article()
alpha_garch_dynamic_plot <- ggplot(all_alphas_dynamic) + geom_line(aes(x=date,y=alpha_garch)) + xlab("") + ylab("Garch") + theme_article()
alpha_rv_dynamic_plot <- ggplot(all_alphas_dynamic) + geom_line(aes(x=date,y=alpha_rv)) + xlab("") + ylab("RV") + theme_article()

all_alphas_dynamic_plot <- grid.arrange(alpha_rolling_dynamic_plot, alpha_garch_dynamic_plot, alpha_rv_dynamic_plot, ncol=1)
ggsave("alphas_plot2.pdf",plot=all_alphas_dynamic_plot,width = 18, height = 9, units= "cm", dpi = 300)

# Calculate Sharpe Ratios
relevant_returns_dynamic <- returns %>% filter(date>="1999-06-29") %>%  select(-date)

alpha_returns_rolling_dynamic <- all_alphas_dynamic$alpha_rolling * relevant_returns_dynamic 
alpha_returns_garch_dynamic <- all_alphas_dynamic$alpha_garch * relevant_returns_dynamic
alpha_returns_rv_dynamic <- all_alphas_dynamic$alpha_rv * relevant_returns_dynamic

dynamic_sharpe_rolling <- mean(alpha_returns_rolling_dynamic$return)/sd(alpha_returns_rolling_dynamic$return)*sqrt(250)
dynamic_sharpe_garch <- mean(alpha_returns_garch_dynamic$return)/sd(alpha_returns_garch_dynamic$return)*sqrt(250)
dynamic_sharpe_rv <- mean(alpha_returns_rv_dynamic$return)/sd(alpha_returns_rv_dynamic$return)*sqrt(250)

# Output Sharpe Ratios
round(sharpe_rolling,2)
round(sharpe_garch,2)
round(sharpe_rv,2)
round(dynamic_sharpe_rolling,2)
round(dynamic_sharpe_garch,2)
round(dynamic_sharpe_rv,2)